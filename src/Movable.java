/**
 * File: Movable.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.SlickException;

/**
 * Used to differentiate between movable and non-movable sprites
 */
public abstract class Movable extends Sprite {
	
	/**
	 * Movable Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Movable(float x, float y) throws SlickException {
		super(x, y);
	}
}
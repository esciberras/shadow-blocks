/**
 * File: Crackedwall.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * The Crackedwall sprite is designed to be destroyed by tnt
 */
public class CrackedWall extends Tile{

	/**
	 * Crackedwall Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public CrackedWall(float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/cracked_wall.png");
		this.setType("Cracked");
	}
	
	/**
	 * Detonates the tnt and self destructs when pushed into the cracked wall
	 * @param input key press from user
	 * @param delta Time passed since last frame (milliseconds).
	 * @throws SlickException
	 */
	public void update(Input input, World world, int delta) throws SlickException {
		
		Sprite tnt;
		
		if ((tnt = world.getSpriteOfType( new String[] {"Tnt"}, this.getX(), this.getY())) != null  ) {
		
			((Tnt) tnt).explode();
			world.destroySprite(this);
		}
	}

}

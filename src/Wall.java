/**
 * File: Wall.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.SlickException;

public class Wall extends Tile {

	/**
	 * Wall Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Wall(float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/wall.png");
		this.setType("Wall");
	}	

}

/**
 * File: Stone.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.SlickException;

public class Stone extends Pushable{
	
	/**
	 * Stone Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Stone( float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/stone.png");
		this.setType("Stone");
	}
	
	/**
	 * Moves the Stone in the specified direction only if it can
	 * @param World
	 * @param Direction
	 */
	public void push(World world,Direction dir) {
		this.attemptMove(world, dir);	
	}
	
}

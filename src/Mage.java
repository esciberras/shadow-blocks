/**
 * File: Mage.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Mage extends Monster{
	
	/**
	 * Mage Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Mage( float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/mage.png");
		this.setType("Mage");
	}
	
	/**
	 * Constantly checks if touching player
	 * @param input key press from user
	 * @param world reference to world class
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input,World world,int delta) throws SlickException {
		
		if(world.getSpriteOfType( new String[] {"Player"} , this.getX(), 
				this.getY()) != null) {
			world.restart();
		}
		Sprite player = world.getSpriteOfType(new String[] {"Player"});
		if (collision(player) ) {
			world.restart();	
			
		}
	}
	
	/**
	 * Handles the Mage Movement Algorithm 
	 * time a player moves and the pushing of blocks
	 * @param world reference to world
	 */
	public void move(World world) {
		Direction dir;
		Sprite player;
		player = world.getSpriteOfType(new String[] {"Player"});
		float distanceX = player.getX() - this.getX();
		float distanceY = player.getY() - this.getY();
		
		// calls from monster class (records rogues and players
		// position, useful for handling the 'walk though' edge case)
		trackMovements(world);
		
		// gets direction as outlined by the spec
		if(Math.abs(distanceX) > Math.abs(distanceY)) {
			dir = (Math.signum(distanceX)==-1) ? Direction.LEFT : Direction.RIGHT;
		}
		else {
			dir = (Math.signum(distanceY)==1) ? Direction.DOWN : Direction.UP;
		}
		
		attemptMove(world,dir);
	}
}

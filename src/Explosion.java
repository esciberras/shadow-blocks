/**
 * File: Explosion.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;


/**
 * The Explosion Class handles the effect when the tnt is
 * pushed into the cracked wall and lasts for 0.4 seconds
 */
public class Explosion extends Effects{
	
	private int lifetime=400;
	
	/**
	 * Explosion Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Explosion(float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/explosion.png");
		this.setType("Explosion");	
	}
	
	/**
	 * Handles the self destruction of the explosion effect
	 * @param input key press from user
	 * @param world reference to world class
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, World world, int delta) throws SlickException {	
		lifetime-= delta;
		if(lifetime<=0) {
			world.destroySprite(this);
		}
	}
}

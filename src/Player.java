/**
 * File: Player.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * This class manages the player sprite  
 * which is directly controlled by the user 
 */

public class Player extends Undoable {
	
	/**
	 * Player Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Player(float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/player_left.png");
		this.setType("Player");
	}
	
	/**
	 * This is the 'main' update method as it converts the input to a direction,
	 * handles pushing of blocks and directing the monsters to respond 
	 * @param input key press from user
	 * @param world reference to world class
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, World world ,int delta) throws SlickException {	
		
		Direction dir = Direction.NONE;
		Sprite spriteToPush;
		
		// Converts the key press to a direction
		if (input.isKeyPressed(Input.KEY_LEFT)) {
			dir = Direction.LEFT;
		}
		else if (input.isKeyPressed(Input.KEY_RIGHT)) {
			dir = Direction.RIGHT;
		}
		else if (input.isKeyPressed(Input.KEY_UP)) {
			dir = Direction.UP;
		}
		else if (input.isKeyPressed(Input.KEY_DOWN)) {
			dir = Direction.DOWN;
		}
		
		int deltaX = dir.getdeltaX();
		int deltaY = dir.getdeltaY();
		
		// Pushes the blocks ("Ice","Stone","Tnt"), warns the 
		// monsters ("Rogue","Skeleton","Mage") then tries to move
		if(dir != Direction.NONE){
			 
			// Calls the undo functionality to add all the sprites previous moves
			world.addPreviousMove();
			
			if( (spriteToPush = world.getSpriteOfType( new String[]{"Ice","Stone","Tnt"}, 
					this.getX()+ deltaX , this.getY()+ deltaY)) != null ) {
				((Pushable) spriteToPush).push(world,dir);
			}

			if( (spriteToPush = world.getSpriteOfType(new String[] {"Rogue","Skeleton","Mage"})) != null ) {
				((Monster) spriteToPush).move(world);
			}
			
			attemptMove(world, dir);
			 
		}
	}
}
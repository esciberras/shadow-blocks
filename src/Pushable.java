/**
 * File: Pushable.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.SlickException;

abstract public class Pushable extends Undoable{
	
	/**
	 * Pushable Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Pushable( float x, float y) throws SlickException {
		super(x, y);
	}
	
	/**
	 * Default behaviour is to try to move
	 * @param World 
	 * @param Direction
	 */
	public void push(World world,Direction dir) {
		this.attemptMove(world, dir);	
	}

}

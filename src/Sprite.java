/**
 * File: Sprite.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * The Sprites Class holds all the attributes and methods 
 * for the items that appear on the screen 
 */
abstract public class Sprite  {
	
	private Image image;
	private float x;
	private float y;
	private String type;
	
	/**
	 * Sprite Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Sprite(float x, float y) throws SlickException{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Checks if the move a sprite wants to make is blocked 
	 * and makes the move if it can, also returns a boolean 
	 * based on if the move was made
	 * @param world
	 * @param dir
	 * @return boolean (True if move is made, false otherwise)
	 */
	public boolean attemptMove(World world, Direction dir) {
		// Translate the direction to an x and y displacement
		float delta_x = dir.getdeltaX(),
			  delta_y = dir.getdeltaY();
	    // Make sure the position isn't occupied!
		if (!this.isSpriteBlocked(world, this.getX() + delta_x, this.getY() + delta_y)) {
			x += delta_x;
			y += delta_y;
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Checks if current tile is occupied by certain sprites
	 * @param world
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @return boolean (True if blocked, false otherwise)
	 */
	public boolean isSpriteBlocked(World world, float x,float y ) {
		Sprite sprite;
		if (Loader.isBlocked(x,y)){
			return true;	
		}
		// Note: The comparison between 'sprite' and 'this' is intentional
		// I want to compare the location in memory
		if( (sprite = world.getSpriteOfType( new String[]{"Ice", "Stone", "Tnt","Cracked","Door"} , x, y))
			!= null && !(sprite == this) ) {
			
			// edge case where to allow a sprite to move if the door is unlocked
			if(sprite instanceof Door && ! ((Door) sprite).isLocked()) {
				return false;
			}
			return true;
		}
		return false;	
	}
	
	/**
	 * Updates Sprites (by default do nothing)
	 * @param input
	 * @param world
	 * @param delta
	 * @throws SlickException
	 */
	public void update(Input input, World world,int delta) throws SlickException {	
	}
	
	/**
	 * Draw sprites on screen
	 * @throws SlickException
	 */
	public void render() throws SlickException {
		if(this.image!=null) {
			(this.image).drawCentered(this.x,this.y);
		}	
	}

	// Getters and Setters //
	
	/**
	 * Set image
	 * @param image_name
	 * @throws SlickException
	 */
	public void setImage(String image_name) throws SlickException {
		
		if (image_name != null) {
			Image image_src = new Image(image_name);
			this.image = image_src;
		}
	}
	
	/**
	 * Returns image
	 * @return Image
	 */
	public Image getImage() {
		return this.image;
	}
	
	/**
	 * Returns x position 
	 * @return x sprite's x coordinate on screen
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * Returns y position
	 * @return y sprite's y coordinate on screen
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Sets sprite's x position
	 * @param x sprite's x coordinate on screen
	 */
	protected void setX(float x) {
		this.x = x;
	}
	
	/**
	 * Sets sprite's y position
	 * @param y sprite's y coordinate on screen
	 */
	protected void setY(float y) {
		this.y = y;
	}
	
	/**
	 * Returns the 'type' (a tag) of the sprite
	 * @return type sprite's name
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Sets the 'type' (a tag) of the sprite
	 * @param type sprite's name
	 */
	protected void setType(String type) {
		this.type = type;
	}
	
}	
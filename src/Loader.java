/**
 * File: Loader.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import java.io.FileReader;
import java.util.ArrayList;
import org.newdawn.slick.SlickException;
import java.io.BufferedReader;

/**
 * The loader class handles the creation of the level from a file
 */
public class Loader {	
	
	private static String[][] types; 
	private static int worldWidth;
	private static int worldHeight;
	private static int offset_x;
	private static int offset_y;

	/**
	 * returns if that location is a blocked tile
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @return boolean 
	 */
	public static boolean isBlocked(float x, float y) {
		x -= offset_x;
		x /= App.TILE_SIZE;
		y -= offset_y;
		y /= App.TILE_SIZE;
		
		// Rounding is important here
		x = Math.round(x);
		y = Math.round(y);
		
		// Do bounds checking!
		if (x >= 0 && x < worldWidth && y >= 0 && y < worldHeight) {
			return types[(int)x][(int)y].equals("wall");
		}
		// Default to blocked
		return true;
	}

	/**
	 * Loads the sprite from a given file.
	 * @param filename
	 * @return ArrayList<Sprite> 
	 */
	public static ArrayList<Sprite> loadSprites(String filename) {
		
		// No magic numbers here....
		final int NAME_LOCATION = 0;
		final int X_POSITION_LOCATION = 1;
		final int Y_POSITION_LOCATION = 2;
		final int WIDTH_ARRAY_LOCATION = 0;
		final int HEIGHT_ARRAY_LOCATION = 1;
		
		ArrayList<Sprite> sprites = new ArrayList<>();
		
		String text;
		
		try (BufferedReader file = new BufferedReader(new FileReader(filename))) {
			 		
		    // Get the length and width from first line
			text = file.readLine();
			
			// All static variables in the loader class 
			worldHeight = Integer.parseInt((text.split(","))[HEIGHT_ARRAY_LOCATION]);
		    worldWidth = Integer.parseInt((text.split(","))[WIDTH_ARRAY_LOCATION]);
		    types = new String[worldWidth][worldHeight];
		    offset_x = (App.SCREEN_WIDTH - worldWidth * App.TILE_SIZE) / 2;
			offset_y = (App.SCREEN_HEIGHT - worldHeight * App.TILE_SIZE) / 2;
			 		
		    	while ((text = file.readLine()) != null) {
		    		
		    		// Get the values from the CSV line 
		    		String name =  (text.split(","))[NAME_LOCATION];
		    		
		    		// Tile position is the CSV value 
		            float x = Integer.parseInt((text.split(","))[X_POSITION_LOCATION]);
		            float y = Integer.parseInt((text.split(","))[Y_POSITION_LOCATION]);
		            
		            // figures out what type of sprite we have and allocates it accordingly
		            // also tells the tiles array if that tile is blocked or not
		            types[(int) x][(int) y] = name;
		            sprites.add(createSprite(name,offset_x + x * App.TILE_SIZE,offset_y + y * App.TILE_SIZE));
		        }
		} catch (Exception e) {
            e.printStackTrace();
	        return null;
		}
		return sprites;
	}
	
	/**
	 * Creates the appropriate sprite based on name
	 * @param name name of sprite to be created
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @return Sprite the created sprite
	 * @throws SlickException
	 */
	private static Sprite createSprite(String name, float x, float y) throws SlickException {
		
		switch (name) {
			case "wall":
				return new Wall(x, y);
			case "floor":
				return new Floor(x, y);
			case "stone":
				return new Stone(x, y);
			case "target":
				return new Target(x, y);
			case "player":
				return new Player(x, y);
			case "cracked":
				return new CrackedWall(x,y);
			case "tnt":
				return new Tnt(x,y);
			case "ice":
				return new Ice(x,y);
			case "skeleton":
				return new Skull(x,y);	
			case "rogue":
				return new Rogue(x,y);	
			case "mage":
				return new Mage(x,y);
			case "door":
				return new Door(x,y);
			case "switch":
				return new Switch(x,y);
		}
		return null;
	}

}

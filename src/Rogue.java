/**
 * File: Rogue.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * The rogue Sprite moves left/right though the level 
 * everytime the player moves and can push blocks
 * @author Eric Sciberras
 *
 */
public class Rogue extends Monster{
	
	private boolean dirLeft=true;
	
	/**
	 * Rogue Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Rogue( float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/rogue.png");
		this.setType("Rogue");
	}
	
	/**
	 * Constantly checks if touching player
	 * @param input key press from user
	 * @param world reference to world class
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, World world,int delta) throws SlickException {
		
		Sprite player;
	
		if(world.getSpriteOfType( new String[] {"Player"} , this.getX(), 
				this.getY()) != null) {
			world.restart();	
		}	
		player = world.getSpriteOfType(new String[] {"Player"});

		// checks if the player and rogue walk through each other
		if (collision(player) ) {
			world.restart();	
		}
	}
	
	/**
	 * Handles the left right movement of the rogue every 
	 * time a player moves and the pushing of blocks
	 * @param world reference to world
	 */
	public void move(World world) {
		Sprite spriteToPush;
		
		Direction dir = (dirLeft) ? Direction.LEFT: Direction.RIGHT;
		int deltaX = dir.getdeltaX();
		
		// calls from monster class (records rogues and players
		// position, useful for handling the 'walk though' edge case)
		trackMovements(world);
		// push blocks
		if( (spriteToPush = world.getSpriteOfType( new String[]{"Ice","Stone","Tnt"}, 
				this.getX()+ deltaX , this.getY())) != null ) {
			((Pushable) spriteToPush).push(world,dir);
		}
		// try to move, if can't reverse direction and try again
		if(!this.attemptMove(world,dir)) {
			dirLeft = ! dirLeft;
			dir = (dirLeft) ? Direction.LEFT: Direction.RIGHT;
			this.attemptMove(world, dir);
		}		
	}

}

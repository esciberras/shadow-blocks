/**
 * File: Target.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Target extends Tile {

	private boolean isBlocked=false;
	
	/**
	 * Target Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Target(float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/target.png");
		this.setType("Target");
	}

	/**
	 * Checks if blocked by and ice or stone tile
	 * @param input
	 * @param world
	 * @param delta
	 * @throws SlickException
	 */
	public void update(Input input, World world,int delta) throws SlickException {
		
		if (world.getSpriteOfType( new String[] {"Ice","Stone"}, this.getX(), this.getY()) != null ) {
			isBlocked = true;	
		}
		else {
			isBlocked = false;
		}
	}
	
	/**
	 * Returns isblocked
	 * @return isblocked
	 */
	public boolean getIsBlocked() {
		return isBlocked;
	}

}

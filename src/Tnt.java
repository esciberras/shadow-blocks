/**
 * File: Tnt.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;


/**
 * The Tnt class is designed to explode and be destroyed
 * if pushed into a cracked Wall
 * @author Eric Sciberras
 *
 */
public class Tnt extends Pushable{
	
	private boolean isDetonated = false;
	
	/**
	 * Tnt Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Tnt( float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/tnt.png");	
		this.setType("Tnt");
	}
	
	/**
	 * Handles the creation of the explosion if the tnt goes into a crackedwall
	 * @param input key press from user
	 * @param world reference to world class
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, World world, int delta) throws SlickException {	
		if(isDetonated) {
			Explosion explosion = new Explosion(this.getX(),this.getY());
			world.createSprite(explosion);
			world.destroySprite(this);
		}	
	} 
	
	/**
	 * Allows the tnt to go into the cracked wall
	 * @param world reference to world 
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 */
	public boolean isSpriteBlocked(World world,float x,float y) {
		
		if (super.isSpriteBlocked(world, x, y) && isDetonated) {
			return true;
		}
		return false;	
	}
	
	/**
	 * Sets isDetonated to true
	 * @throws SlickException
	 */
	public void explode() throws SlickException {
		isDetonated = true;
	} 
	
}

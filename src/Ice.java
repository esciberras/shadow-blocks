/**
 * File: Ice.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;


/**
 * Manages the ice sprite which is designed to keep moving a
 * tile every 0.25 seconds until it reaches a blocked area
 */

public class Ice extends Pushable{

	private int time = 0;
	private boolean moving = false;
	private Direction dir = Direction.NONE;
	private static final int delaytime = 250; 
	
	/**
	 * Ice Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Ice( float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/ice.png");
		this.setType("Ice");
	}
	
	/**
	 * The ice block will keep moving until it hits a blocked area
	 * @param input key press from user
	 * @param world reference to world class
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input,World world,int delta) throws SlickException {
		
		if(moving) {
			time+= delta;
			if(time >= delaytime) {
				if(!this.attemptMove(world,dir)) {
					moving=false;
					time = 0;
					dir = Direction.NONE;
				}
			time=0;
			}	
		}
	}
	
	/** 
	 * Handles the initial push of the ice by a unit
	 * @param dir Direction to move
	 * @param world reference to world class
	 */
	public void push(World world,Direction dir) {
		this.dir=dir;
		moving=true;
		attemptMove(world, dir);	
	}
	
	/**
	 * Ice's addX method will add it's position if it's not moving 
	 * otherwise it will add a null value (just a placeholder)
	 * @param x sprite's x coordinate on screen
	 */
	public  void addX(float x) {
		if(!moving) {
			xStack.push(x);
		}
		else {
			xStack.push(null);
		}
	}
	
	/**
	 * Ice's addX method will add it's position if it's not moving 
	 * otherwise it will add a null value (just a placeholder)
	 * @param y sprite's x coordinate on screen
	 */
	public  void addY(float y) {
		if(!moving) {
			yStack.push(y);
		}
		else {
			yStack.push(null);
		}
	}
	
	/**
	 * Ice's undo method will pop out the null values (when it was moving)
	 * and call the super method when there's valid values in the stack
	 */
	public void undo() {
		moving=false;
		if(!this.xStack.isEmpty() && xStack.peek() == null) {
			xStack.pop();
			yStack.pop();	
		}
		else {
			super.undo();
		}
	}
		
}

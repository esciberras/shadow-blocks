/**
 * File: Skull.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * The skull class moves one tile per second up and down the level
 */

public class Skull extends Movable{
	
	private int time = 0;
	private boolean dirUp = true;
	private static int timeBetweenMovement = 1000;
	
	/**
	 * Skull Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Skull( float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/skull.png");
		this.setType("Skull");
	}
	
	/**
	 * Handles the up and down movement of the skull
	 * @param input key press from user
	 * @param world reference to world class
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input,World world,int delta) throws SlickException {
		this.time+= delta;
		
		if(this.time >=  timeBetweenMovement) {
			// if blocked reverse direction and try to move again
			if(!this.attemptMove(world,(dirUp) ? Direction.UP: Direction.DOWN)) {
				dirUp = ! dirUp;
				this.attemptMove(world,(dirUp) ? Direction.UP: Direction.DOWN);
			}
			this.time=0;
		}
		
		// if touches player restart level
		if(world.getSpriteOfType( new String[] {"Player"} , this.getX(), 
				this.getY()) != null) {
			world.restart();
			
		}
	}
}
/**
 * File: Tile.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.SlickException;

/**
 * Boring old Tile Class
 */
abstract public class Tile extends Sprite{
	
	/**
	 * Tile Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Tile( float x, float y) throws SlickException {
		super(x, y);
	}
	
}
/**
 * File: Direction.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

/**
 *  The Direction enum holds the x and y values for movement 
 */
public enum Direction {
	NONE(0,0),
	LEFT(-App.TILE_SIZE,0),
	RIGHT(App.TILE_SIZE,0),
	UP(0,-App.TILE_SIZE),
	DOWN(0,App.TILE_SIZE);
	
	public final int deltaX;
	public final int deltaY;
	
	private Direction(int deltaX,int deltaY){
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }
	
	/**
	 * Returns the movement in the x coordinate
	 * @return deltaX the movement required by a direction
	 */
    public int getdeltaX(){
        return this.deltaX;
    }
    
    /**
	 * Returns the movement in the y coordinate
	 * @return deltaY the movement required by a direction
	 */
    public int getdeltaY(){
        return this.deltaY;
    }
    
}

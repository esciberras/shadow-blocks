/**
 * File: Effects.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.SlickException;

/**
 * Just an abstraction for the game to hold effects
 * this implementation only has an explosion effect
 */

public abstract class Effects extends Sprite {
	
	/**
	 * Effects Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Effects(float x, float y) throws SlickException {
		super(x, y);
	}
}

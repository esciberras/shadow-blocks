/**
 * File: World.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import java.util.ArrayList;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * World Class holds all the sprites and is where our game board lives
 */

public class World {	
	
	private ArrayList<Sprite> sprites;
	private int moves=0;
	private int level=1;
	private static final int NUMBER_OF_LEVELS = 6;
	
	public World() {
		sprites = Loader.loadSprites("res/levels/" + level + ".lvl");
	}
	
	public void createSprite(Sprite sprite) {
		sprites.add(sprite);
	}
	
	/**
	 * Iterates Through the sprites Arraylist and removes the Sprite   
	 * @param doomedSprite sprite that is to be deleted
	 */
	public void destroySprite(Sprite doomedSprite) {
		
		 for(int i=0; i < sprites.size() ; i++) {
			 if (sprites.get(i) == (doomedSprite) ) {
					sprites.remove(i);
			 }	
		 }
	}
	
	/**
	 * Handles updating the sprites and calling undo and restart methods
	 * @param input key press from user
	 * @param delta Time passed since last frame (milliseconds).
	 * @throws SlickException
	 */
	public void update(Input input, int delta) throws SlickException{
		// Handles the level changes
		if (this.isFinished()) {
			level++;
			sprites = Loader.loadSprites("res/levels/" + level + ".lvl");
		}
		// undo functionality
		if(input.isKeyPressed(Input.KEY_Z) ) {		
			this.undo();
		}
		// restart functionality
		else if(input.isKeyPressed(Input.KEY_R) ) {		
			restart();
		}
		// update sprites
		for (int i=0; i<sprites.size() ; i++) {
			if (sprites.get(i) != null) {
				sprites.get(i).update(input, this, delta);
			}
		}
	}
	
	/**
	 * Draw the sprites on to the screen
	 * and draw the Moves counter
	 * @param g 
	 * @throws SlickException
	 */
	public void render(Graphics g) throws SlickException {
		
		// Draw each sprite on the map
		for (Sprite sprite : sprites) {
			if (sprite != null) {
				sprite.render();
			}
		}
		g.drawString("Moves " + moves, 0,0);
	}
	
	/**
	 * Restarts level and resets move counter
	 */
	public void restart() {
		sprites = Loader.loadSprites("res/levels/" + level + ".lvl");
		moves = 0;
	}
	
	/**
	 * iterate through undoable sprites, calls their
	 * undo methods and increments down a move
	 */
	public void undo() {
		if(moves!=0) {
			moves--;
		}
		for (Sprite sprite : sprites) {
			if(sprite instanceof Undoable)
				((Undoable) sprite).undo();
		}
	}
	
	/**
	 * iterates through all undoable sprites and 
	 * adds their position to their undo stack
	 */
	public void addPreviousMove() {
		// looks like a random place to add to the move counter but every time
		// the player makes a move the undo stacks need  to be updated 
		moves++;
		for(Sprite sprite: sprites) {
			if (sprite instanceof Undoable) {
	    		((Undoable) sprite).addX(sprite.getX());
				((Undoable) sprite).addY(sprite.getY());
    		}	
		}
	}
	
	
	/**
	 * Checks if all targets are 
	 * blocked or if we are on the last level
	 * @return boolean
	 */
	public boolean isFinished() {
		
		// Find at least one Target that is not blocked
		for(Sprite sprite: sprites) {
			if (sprite instanceof Target && !(((Target) sprite).getIsBlocked()) ) {
				return false;
			}
		}
		// Since The last level doesn't 
		// have any switches return false
		if(level == NUMBER_OF_LEVELS-1) {
			return false;
		} 	
	moves = 0;
	return true;
	}
	
	/**
	 * Finds a Sprite (from the Sprites arraylist) that is in a 
	 * certain position and is of one of the types passed into the types array
	 * @param types a tag used to represent the sprite
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @return Sprite
	 */
	public Sprite getSpriteOfType(String[] types, float x, float y) {

		 for(Sprite sprite: sprites) {
			 for(String type : types) {
				 if ( (sprite.getX() == x) && 
						 (sprite.getY() == y ) && type.equals(sprite.getType()) ) {
						return sprite;
				 } 
			 }	
		 }
		 return null;
	}
	
	/**
	 * Finds a sprite of the type requested in the Sprites arraylist
	 * @param type a tag used to represent the sprite
	 * @return sprite the sprite that is requested 
	 */
	public Sprite getSpriteOfType(String[] types) {

		 for(Sprite sprite: sprites) {
			 for(String type : types) {
				 if (type.equals(sprite.getType()) ) {
						return sprite;
				 } 
			 }	
		 }
		 return null;
	}
	
	/**
	 * Returns the number of moves made
	 * @return int number of moves
	 */
	public int getmoves() {
		return moves;	
	}
		
}

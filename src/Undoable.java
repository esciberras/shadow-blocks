/**
 * File: Undoable.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import java.util.Stack;
import org.newdawn.slick.SlickException;

/**
 *  This class is used to implement the undo functionality. 
 *	Basically the system works by using stacks for each movable sprite that hold the x 
 *	and y positions, every time a move is made all the sprites positions are 
 *	added to the stacks (by the world class) and when an undo is requested the 
 *	positions are taken for the stack and set into the sprites
 */
abstract public class Undoable extends Movable {
	
	Stack<Float> xStack = new Stack<Float>();
	Stack<Float> yStack = new Stack<Float>();
	
	/**
	 * Undoable constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Undoable(float x, float y) throws SlickException {
		super(x, y);
	}
	
	/**
	 * Add to the y undo stack 
	 * @param y sprite's x coordinate on screen
	 */
	public void addX(float x) {
		xStack.push(x);
	}
	
	/**
	 * Add to the y undo stack 
	 * @param y sprite's y coordinate on screen
	 */
	public void addY(float y) {
		yStack.push(y);
	}
	
	/**
	 * Takes the values from the undo stacks and 
	 * sets them as the Sprite's position
	 */
	public void undo() {
		if(!this.xStack.empty() && !this.yStack.empty()) {
			this.setX( xStack.pop() ); 
			this.setY( yStack.pop() );	
		}		
	}
	
	/**
	 * Removes all the previous moves from the undo stacks
	 */
	public void clearStacks () {
		while(!this.xStack.empty() && !this.yStack.empty()) {
			xStack.pop();
			yStack.pop();
		}
	}
	
}

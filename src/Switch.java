/**
 * File: Switch.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Switch extends Tile{
	
	/**
	 * Switch Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Switch( float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/switch.png");
		this.setType("Switch");
	}
	
	/**
	 * The Switch update handles the opening and closing of the door
	 * @param input key press from user
	 * @param world reference to world class
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, World world,int delta) throws SlickException {
		
		Sprite door;
		
		// if a switch is blocked open door, otherwise close door
		if ( (world.getSpriteOfType( new String[] {"Ice","Stone"}, this.getX(), this.getY()) != null)
				&& (door = world.getSpriteOfType(new String[] {"Door"})) != null  ) {
			((Door) door).openDoor();
		}
		else if( (door = world.getSpriteOfType(new String[] {"Door"})) != null) {
			((Door) door).closeDoor();
		}
	}

}

/**
 * File: Door.java
 * @author Eric Sciberras
 * With help from project 1 and 2A solution by Eleanor McMurtry.
 */

import org.newdawn.slick.SlickException;

/**
 * The door sprite is designed to open when a 
 * switch is covered by an ice or stone block
 */
public class Door extends Tile{
	
	private boolean locked = true; 
	
	/**
	 * Crackedwall Constructor
	 * @param x sprite's x coordinate on screen
	 * @param y sprite's y coordinate on screen
	 * @throws SlickException
	 */
	public Door( float x, float y) throws SlickException {
		super(x, y);
		this.setImage("/res/door.png");
		this.setType("Door");
	}
	
	/**
	 * Only renders if door is 'Locked'
	 */
	public void render() throws SlickException {
		if(locked) {
			(this.getImage()).drawCentered(this.getX(),this.getY());
		}
	}
	
	/**
	 * Sets locked to false 
	 */
	public void openDoor() {
		locked = false;
	}
	
	/**
	 * Sets locked to true 
	 */
	public void closeDoor() {
		locked = true;
	}
	
	/**
	 * Returns attribute locked
	 * @return boolean 
	 */
	public boolean isLocked() {
		return locked;
	}

}

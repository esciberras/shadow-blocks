# README #

Shadow blocks is a game i coded to a specification for a university assignment. The game involves completing various puzzles to progress to the next stage.
Some of the mechanics used are:

- **pressure plates:** that when covered by movable blocks can trigger to open doors or progress to the next level.
- **Tnt** When pushed into a cracked wall, makes the wall explode
- **undo function:** allows the player to change their mind if a mistake is made
- **enemies:** various adversaries that move either when the player moves, every second or try to actively follow you

### How do I get set up? ###

This Game is run through slick, however all dependencies are included. The recommended way to install would be by:

1. clone the repo
2. import into eclipse
3. right click the project -> go to run as -> click java application
